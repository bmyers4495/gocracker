package main

import (
	"net/http"
	"net/http/cookiejar"
	"sync"
)

func main() {

	userfile := "./passlists/testuser"
	passfile := "./passlists/testpass"
	jar, _ := cookiejar.New(nil)
	app := App{
		Client: &http.Client{Jar: jar},
	}
	var wg sync.WaitGroup
	wg.Add(1)
	go getlines(userfile, &usernameList, &wg)
	wg.Add(1)
	go getlines(passfile, &passwordList, &wg)
	wg.Wait()
	for i := 0; i <= len(usernameList)-1; i++ {
		wg.Add(1)
		go testCreds(&app, usernameList[i], &wg)
	}
	wg.Wait()

}

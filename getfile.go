package main

import (
	"bufio"
	"log"
	"os"
	"sync"
)

func getlines(readFile string, gList *[]string, wg *sync.WaitGroup) {
	defer wg.Done()
	file, err := os.Open(readFile)
	defer file.Close()
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		*gList = append(*gList, scanner.Text())
	}
}
